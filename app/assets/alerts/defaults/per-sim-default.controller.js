'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('DefaultPerSIMAlertsCtrl', DefaultPerSIMAlerts);

    DefaultPerSIMAlerts.$inject = ['$scope', '$compile', 'loadingState', 'dataTableConfigService', 'alertsService', 'commonUtilityService','alertsCommonFactory','$location', 'analytics','pollingAPIService','$interval'];

    /**
     * @func DefaultPerSIMAlerts()
     * @param $scope
     * @param $compile
     * @param loadingState
     * @param dataTableConfigService
     * @param alertsService
     * @param commonUtilityService
     * @param alertsCommonFactory
     * @param $location
     * @param analytics
     * @constructor
     */
    function DefaultPerSIMAlerts($scope, $compile, loadingState, dataTableConfigService, alertsService, commonUtilityService,alertsCommonFactory,$location, analytics,pollingAPIService,$interval) {

        var defaultPerSIMAlerts = this;
        var navAlertPresent = false;
        var currentQueryParam = {};
        var promise;

        /**
         * Start up logic for controller
         */
        function activate(){
            $scope.emptyTableMessage = "No actions available.";
            // Functions for bulk options
            defaultPerSIMAlerts.confirmAlertsEnable = alertsCommonFactory.confirmAlertsEnable;
            defaultPerSIMAlerts.confirmAlertsDisable = alertsCommonFactory.confirmAlertsDisable;
            defaultPerSIMAlerts.confirmAlertsDeletion = alertsCommonFactory.confirmAlertsDeletion;

            // DataTables
            addDataTablesObject($scope);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            defaultPerSIMAlerts.alertId = $location.search().alertId;

            // Adding/Editing Alerts
            $scope.alertModalObj = {};
            defaultPerSIMAlerts.showAlertModal = alertsCommonFactory.showAlertModal;

            // Prepare alertsService with controller scope
            alertsCommonFactory.setScope($scope);
            $scope.alerts = {};
            $scope.isDisableComponent = {};

            promise = $interval(function() {alertsCommonFactory.monitorAlertStatus(currentQueryParam);}, 60000);

        }

        $scope.$on('$destroy', function(){
            //cancelling the interval to monitor alert status when controller is getting destroyed
            $interval.cancel(promise);
        });

        activate();

        /**
         * @function addDataTablesObject
         * @param {object} scope - controller $scope
         * @desc adds the dataTable object to the controller as object
         *
         * TODO - Create the functionality for dataTableOptions.fnServerData dynamically.
         */
        function addDataTablesObject(scope){
            scope.dataTableObj = {
                tableID: "defaultPerSIMAlertsDataTable",
                tableHeaders: [
                    {
                        value: "",
                        dbValue: ""
                    },
                    {
                        value: "Allocation",
                        dbValue: ""
                    },
                    {
                        value: "Action Outcome",
                        dbValue: ""
                    },
                    {
                        value: "Triggered",
                        dbValue: ""
                    },
                    {
                        value: "Status",
                        dbValue: ""
                    },
                    {
                        value: "",
                        dbValue: ""
                    }
                ],
                dataTableOptions: {
                    bPaginate: false,
                    bInfo: false,
                    columns: [
                        {
                            width: "60px",
                            orderable: false
                        },
                        {
                            width: "130px",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            className: "checkbox-table-cell",
                            render: function ( data, type, row) {
                                return '<div gs-has-permission="EDIT_ALERT,DELETE_ALERT" class="checkbox check-success large no-label-text hideInReadOnlyMode"><input type="checkbox" value="1" id="checkbox_'+row.alertId+'"><label ng-click="dataTableObj.selectRow($event)" id="label_'+row.alertId+'"></label></div>';
                            }
                        },
                        {
                            targets: 1,
                            render: function (data, type, row) {
                                return row.alertSpecification.limitValue;
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, row) {
                                var action = alertsCommonFactory.getActionName(row.alertSpecification.actions[0]);
                                return action;
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, row) {
                                $scope.alerts[row.alertId] = row.triggerCount;
                                return '{{alerts['+row.alertId+']}}<i class="fa fa-spinner fa-pulse" style="margin-left: 5px" ng-show="isDisableComponent['+row.alertId+']"></i>';
                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, row) {
                                if(row.alertSpecification.enable)
                                    return '<div class="nowrap"><span class="status active"></span><span class="status-text">Enabled</span></div>';
                                return '<div class="nowrap"><span class="status inactive"></span><span class="status-text">Disabled</span></div>';
                            }
                        },
                        {
                            targets: 5,
                            render: function (data, type, row, meta) {
                                return '<a gs-has-permission="EDIT_ALERT" id="edit_alert_'+row.alertId+'" ng-click="showEditAlertModal('+meta.row+')" href="javascript:void(0)" title="Click to edit the alert" class="alerts-edit nowrap hideInReadOnlyMode"><i class="fa fa-pencil edit-ico"></i> Edit</a>';
                            }
                        }
                    ],
                    createdRow: function ( row, data ) {
                        $(row).attr("id",'ALERTID_'+data.alertId);
                        $scope.isDisableComponent[data.alertId] = false;
                        if(data.alertId == defaultPerSIMAlerts.alertId){
                            navAlertPresent = true;
                            $(row).addClass('highlighted-row');
                        }
                        $compile(row)($scope);
                        if(data.alertHistoryActionStatus== "IN_PROGRESS") {
                            $scope.isDisableComponent[data.alertId] = true;
                            pollingAPIService.monitor(data.alertId, alertsService.getAlert, alertsCommonFactory.pollStop, alertsCommonFactory.pollCallback);
                        }
                    },
                    fnServerData: function (sSource, aoData, fnCallback) {

                        var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                        queryParam.alertType= 'DEFAULT_PER_SIM_PER_ACCOUNT';
                        currentQueryParam = queryParam;
                        alertsService.getAlerts(queryParam).success(function(response){

                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            //angular.element('#checkbox_action').prop('checked', false);
                            var tableData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);

                            // Store the data to the scope
                            $scope.defaultPerSIMAlertsList = tableData.aaData;

                            // Call the dataTable with the appropriate data
                            if(oSettings != null)
                                fnCallback(tableData);
                            else
                                loadingState.hide();

                        }).error(function(data){
                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"actions");
                            commonUtilityService.showErrorNotification(data.errorStr);
                        });
                    },
                    "fnDrawCallback": function(oSettings) {
                        if(defaultPerSIMAlerts.alertId && !navAlertPresent){
                            commonUtilityService.showErrorNotification("For selected alert, action is not available.Clearing the corresponding alert is in progress.");
                        }
                       // dataTableConfigService.disableTableHeaderOps($scope.defaultPerSIMAlertsList.length, oSettings);
                    },
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ ",
                        "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                        "sEmptyTable": $scope.emptyTableMessage,
                        "sZeroRecords":$scope.emptyTableMessage
                    }
                }
            };
        }

        $scope.showEditAlertModal = function(index){
            var config = {
                alertType: 'DEFAULT_PER_SIM_PER_ACCOUNT'
            };
            defaultPerSIMAlerts.showAlertModal(config, $scope.defaultPerSIMAlertsList[index]);
        };

        $scope.addAlert = function(alert){

            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'DEFAULT_PER_SIM_PER_ACCOUNT','DATA');

            var alertConfig = {};
            alertConfig.type = 'AlertConfigurationList';
            alertConfig.list = [alertObj];

            alertsService.addAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Action has been added");

                analytics.sendEvent({
                    category: "Operations",
                    action: "Add Per SIM Default Action",
                    label: "Actions"
                });

            }).error(function(data){
                console.log('add alert failure' + data);
                alertsCommonFactory.showError(data,'DEFAULT_PER_SIM_PER_ACCOUNT');

            });
        };

        $scope.editAlert = function(alert){
            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'DEFAULT_PER_SIM_PER_ACCOUNT','DATA');
            alertObj.alertId = alert.id;

            var alertConfig = {};
            alertConfig.type = 'EditAlertsRequest';
            alertConfig.list = [alertObj];

            alertsService.editAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Action has been updated");

                analytics.sendEvent({
                    category: "Operations",
                    action: "Edit Per SIM Default Action",
                    label: "Actions"
                });

            }).error(function(data){
                console.log('update alert failure' + data);
                alertsCommonFactory.showError(data,'DEFAULT_PER_SIM_PER_ACCOUNT');
            });
        };

    }
})();