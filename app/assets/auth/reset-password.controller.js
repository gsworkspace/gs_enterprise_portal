'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('resetPasswordCtrl', ResetPassword);

    ResetPassword.$inject = ['$scope','$stateParams','$sce','AuthenticationService', 'loadingState'];

    /**
     @constructor Login()
     @param {object} $scope - Angular.js $scope
     @param {object} $location - Angular.js $location service
     @param {object} formMessage - Factory object
     */
    function ResetPassword($scope,$stateParams,$sce,authenticationService,loadingState){

        var resetPwd = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            resetPwd.resetPassword = resetPassword;
            resetPwd.title = 'Reset Password';

            resetPwd.isTokenValid = true;
            resetPwd.errorMessage ='';
            resetPwd.successMessage = '';
        }

        function resetPassword(rsertForm,event) {

            if(event && event.which != 13){
                return;
            }

            if ($scope.isFormValid(rsertForm)) {
                authenticationService.resetPassword(rsertForm.newPassword.$modelValue, $stateParams.data).success(function (response) {
                    resetPwd.isTokenValid = false;
                    resetPwd.message = $sce.trustAsHtml('Your account password has been changed. Please <a href="#/auth/login">log in</a> with new credentials.');

                }).error(function (response) {
                    if(response.errorStr == 'Invalid token'){
                        resetPwd.isTokenValid = false;
                        resetPwd.message = $sce.trustAsHtml('Reset password link has expired. Please <a href="#/auth/forgot-password">Click here</a> to generate link again.');
                    }else{
                        resetPwd.errorMessage = response.errorStr;
                    }
                });
            }
        }

        activate();
        loadingState.hide();
    }
})();