'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('ExportCtrl', ExportTableData);

	ExportTableData.$inject=['$scope','modalDetails'];

	/**
		@constructor ExportTableData()
		@param {object} $scope - Angular.js $scope
	 	@param {object} modalDetails - Factory service to setup modal object
	*/   
	function ExportTableData($scope, modalDetails){

		var exportModal = this;

		/**
		 * @func activate()
		 * @desc Start up logic for controller
		 */
		function activate(){
			addModalAttributes();
			$scope.$parent.exportModal = controlModalDisplay;
		}

		activate();

		/**
		 * @func addModalAttributes()
		 * @desc Adds the id, form name, header, and button text/values. Also adds submit and cancel functions
		 */
		function addModalAttributes(){
			modalDetails.scope(exportModal, true);
			modalDetails.attributes({
				id: "modal-export-data",
				formName: "modal-form-export",
				header: "Export Table Data",
				submit: "Export"
			});
			modalDetails.submit(function(){console.log("Add Submit Function Here");});
			modalDetails.cancel(function(){console.log("Add Cancel Function Here");});
		}

		/**
		 * @func controlModalDisplay
		 * @param {boolean} open - truthy value to control modal display
		 * @param {object} tableObj - Vm reference of the controller
		 * @desc controlModalDisplay allows for a parent controller to control
		 * showing and hiding the modal. Also allows the parent controller to set which table
		 * to select data from
         */
		function controlModalDisplay(open, exportData){
            exportModal.actualExpData = angular.copy(exportData);
			exportModal.exportData = angular.copy(exportData);
			if(exportModal.exportData){
				if(exportModal.exportData.hasOwnProperty('url')){
					exportModal.exportData.count=5000;
					exportModal.exportData.startIndex=0;
					exportModal.exportData.fileType="csv";

                    if(exportModal.exportData.sortBy){
                        exportModal.exportData.sortBy = exportModal.exportData.sortBy;
                    }

                    if(exportModal.exportData.sortDirection){
                        exportModal.exportData.sortDirection = exportModal.exportData.sortDirection;
                    }
					exportModal.exportData.token=sessionStorage.getItem("token");
					exportModal.url=exportModal.exportData.url + joinURLParams(exportModal.exportData);
					console.log(exportModal.url);
				}
			}


			if(open && open == true) {
                resetExportModal();
                $("#" + exportModal.modal.id).modal("show");
            }
			else if(!open && open == false)
				$("#"+exportModal.modal.id).modal("hide");
		}

        /**
         * @function joinURLParams
         * @param obj
         * @returns {string} url encoded string
         * @desc takes key value from object and converts it into url query parameters
         */
		function joinURLParams(obj){
			var str=[];
			for(var key in obj){
				if(key != "url"){
					str.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]));
				}
			}
			return str.join("&");
		}

        /**
         * @function resetExportModal
         * @desc reset the export form fields to default values. Will access the child scope to change these variables
         */
        function resetExportModal(){
            exportModal.childAccess.exportOption=1;
            exportModal.childAccess.exportPageStart=null;
            exportModal.childAccess.exportPageEnd=null;
            exportModal.childAccess.errorStr=false;
			exportModal.childAccess.handleRadioState();
        }

	}
})();
