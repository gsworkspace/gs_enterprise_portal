'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsUpdateGroup',UpdateGroup);

	UpdateGroup.$inject = ['formMessage'];
	
	/**
		@function updateGroup
		@param {object} formMessage - Factory object
	*/		
	function UpdateGroup(formMessage) {
		
		var link = {
			link: link
		};
		
		return link;
		
		/**
			@function link()
			@param {object} $scope - directive $scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element	
		*/
		function link($scope, element, attrs) {
			$scope.$parent.groupModalObj.grpAddModalMessage = setMessage;
			
			/**
				@function setMessage()
				@param {string} msg - message to use for formMessage factory
				@param {string} className - the class name to use for the formMessage
			*/
			function setMessage(msg, className){
				/* Enable a formMessage factory */
				formMessage.scope($scope.groupModalObj);
				formMessage.className(className);
				formMessage.message(true, msg);
			}
		}
	}
})();

/* Directive to reset the previously selected group when invalid group name is entered */
(function(){
	angular.module('app').directive('gsResetSelectedGroup', function() {
			return {
				restrict: 'AE',
				link:function ($scope, element, attrs) {

					console.log( element );
					//scope.selectObj.selected = true;
					//scope.selectObj.open = true;
					if (typeof $scope.$parent.groupModalObj.group != "undefined") delete $scope.$parent.groupModalObj.group;
					if (typeof $scope.$parent.groupModalObj.user != "undefined") delete $scope.$parent.groupModalObj.user.group;


				}

			};
		});
})();
