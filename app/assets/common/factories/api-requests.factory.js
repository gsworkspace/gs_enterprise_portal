'use strict';

(function(){
	angular
		.module('app')
		.factory('APIRequests',['$http', APIRequests]);
	
	/**
		@function APIRequests()
		 - Factory method for accessing the API to send data.
	*/ 	
	function APIRequests($http){
		
		var path = "http://52.88.45.123:3000/api/v1/";
		var all = "?startIndex=0&count=100000000000";

	    var factory = {
	        getData: getData,
	        putData: putData,
	        postData: postData,
	        deleteData: deleteData
	    };
	    
	    return factory;
		
		/**	
			@function getData()
			@param {string} url - relative url to get data from 	
		*/
		function getData(url, isAll){
			var promise;
			if(isAll){
				promise = $http.get(path+url+all);
			} else {
				promise = $http.get(path+url);
			}
			return promise;
		}
		
		/**	
			@function putData()
			@param {string} url - relative url to get data from 	
			@param {object} data - data to update the API with
		*/
		function putData(url, data){
			return $http.put(path+url, data);
		}
		
		/**	
			@function postData()
			@param {string} url - relative url to get data from 	
			@param {object} data - data to update the API with
		*/
		function postData(url, data){
			return $http.post(path+url, data);
		}
		
		/**	
			@function deleteData()
			@param {string} url - relative url to get data from 	
		*/
		function deleteData(url){
			return $http({
				method:"DELETE",
				url:path+url
			});
		}
		
	}	
})();