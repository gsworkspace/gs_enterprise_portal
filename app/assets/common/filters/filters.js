'use strict';

(function () {
    angular.module('app')
        .filter('underscoreless', function () {
            return function (input) {
                if (input)
                    return (''+input).replace(/_+/g, ' ');
                console.log('Filter underscoreless -Input undefined/null');
                return ''
            }
        }).filter('searchPlaceholders', function () {
        return function (tableHeaders) {
            if (angular.isObject(tableHeaders)){
                var searchPlaceholder = '';
                for(var i=0;i<tableHeaders.length;i++){
                    if(tableHeaders[i].search && tableHeaders[i].placeHolder){
                        if(searchPlaceholder){
                            searchPlaceholder+=',';
                        }
                        searchPlaceholder+=tableHeaders[i].placeHolder;
                    }
                }
               return searchPlaceholder;
            }
            console.log('Table Headers should be a Object');
            return ''
        }
    });
})();