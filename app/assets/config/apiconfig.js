var ENTERPRISE_PROD_ENV = 'p';
var ENTERPRISE_LOCAL_ENV = 'l';
var ENTERPRISE_LOCAL_GEM_ENV = 'lb';
var ENTERPRISE_STAGING_ENV = 's';
var ENTERPRISE_CAYMAN_STAGING_ENV = 'cs';
var ENTERPRISE_LOCAL_TEST_ENV = 'lt';

var ENTERPRISE_ENVIRONMENT = "p";
var RBAC_ENABLED=true;
var ALERTS_ENABLED = true;
var DISABLE_CAPTCHA = false;
var ENABLE_RENEW_TOKEN_SUPPORT = false;
var RENEW_TOKEN_THRESHOLD_TIME = 5*60*1000;
var ENABLE_DATA_USAGE_DATE_SELECTION = false;
var ENABLE_MOCK_API = false;
var ENABLE_1_4_1 = true;


var curUrl = window.location.href;

if(curUrl.indexOf("stage")!=-1)
{
    ENTERPRISE_ENVIRONMENT = "cs";
}

var ISDEMO =false;
var GRAPH_COLORS = ["#8A7DBE", "#6DC0F9", "#40D9CA", '#F9D975', '#F77975','#D2DBA4', '#F8A1B1','#EE840A'];
var envOptions = {};
(function initEnvironment(){
    envOptions = {pserver:"https://services-enterprise.gigsky.com",puserver:"https://services-enterprise.gigsky.com",paserver:"https://services-enterprise.gigsky.com",ppserver:"https://services-enterprise.gigsky.com",peserver:"https://services-enterprise.gigsky.com",
        lserver:"http://localhost:3000",luserver:"http://localhost:3000",laserver:"http://localhost:3000",lpserver:"http://localhost:3000",leserver:"http://localhost:3000",
        lbserver:"http://localhost:8080",lbuserver:"http://localhost:8080",lbaserver:"http://localhost:8080",lbpserver:"http://localhost:8080",lbeserver:"http://localhost:8080",
        ltserver:"http://webclienttestnode:8080",ltuserver:"http://webclienttestnode:8080",ltaserver:"http://webclienttestnode:8080",ltpserver:"http://webclienttestnode:8080",lteserver:"http://webclienttestnode:8080",
        sserver:"https://staging.gigsky.com",suserver:"https://staging.gigsky.com",saserver:"https://staging.gigsky.com",spserver:"https://staging.gigsky.com",seserver:"https://staging.gigsky.com",
        csserver:"https://staging-ent.gigsky.com",csuserver:"https://staging-ent.gigsky.com",csaserver:"https://staging-ent.gigsky.com",cspserver:"https://staging-ent.gigsky.com",cseserver:"https://staging-ent.gigsky.com"
    };

    window.ENTERPRISE_SERVER_BASE = envOptions[ENTERPRISE_ENVIRONMENT+'server'];
})();

var ENTERPRISE_API_VERSION = "/api/v1/";
var ENTERPRISE_ANALYTICS_API_VERSION = "/abs/api/v1/";
var ENTERPRISE_GIGSKY_BACKEND_API_VERSION = "/embs/api/v1/";
var ENTERPRISE_GIGSKY_BACKEND_API_VERSION2 = "/embs/api/v2/";
var ENTERPRISE_PLAN_API_VERSION = "/pmbs/api/v1/";
var ENTERPRISE_USER_API_VERSION = "/umbs/api/v1/";

var ENTERPRISE_PLAN_API_VERSION_V3 = "/pmbs/api/v3/";

var ENTERPRISE_API_BASE= ENTERPRISE_SERVER_BASE + ENTERPRISE_API_VERSION;
var ENTERPRISE_ANALYTICS_BASE= envOptions[ENTERPRISE_ENVIRONMENT+'aserver']+ENTERPRISE_ANALYTICS_API_VERSION+'account/';
var ENTERPRISE_GIGSKY_BACKEND_BASE = envOptions[ENTERPRISE_ENVIRONMENT+'eserver'] + ENTERPRISE_GIGSKY_BACKEND_API_VERSION;
var ENTERPRISE_GIGSKY_BACKEND_BASE_2 = envOptions[ENTERPRISE_ENVIRONMENT+'eserver'] + ENTERPRISE_GIGSKY_BACKEND_API_VERSION2;
var ENTERPRISE_PLAN_API_BASE = envOptions[ENTERPRISE_ENVIRONMENT+'pserver'] + ENTERPRISE_PLAN_API_VERSION;
var ENTERPRISE_USER_API_BASE = envOptions[ENTERPRISE_ENVIRONMENT+'userver'] + ENTERPRISE_USER_API_VERSION;

var ENTERPRISE_PLAN_API_BASE_V3 = envOptions[ENTERPRISE_ENVIRONMENT+'pserver'] + ENTERPRISE_PLAN_API_VERSION_V3;

var LAZY_LOAD_BASE = "";

var ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE = "ENTERPRISE_PRO";

ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE = !RBAC_ENABLED?"ENTERPRISE_PRO":ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;


var ENTERPRISE_DEFAULT_PERMISSION_LIST = window[ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE+"_PERMISSION_LIST"];


var ENTERPRISE_ACCOUNT_SUBSCRIPTION_TYPES = [ { "name" :"ENTERPRISE_PRO"}, { "name" : "ENTERPRISE_LITE"}];
var SUB_ACCOUNT_TYPE = "RESELLER_SUB_ACCOUNT";
var RESELLER_ACCOUNT_TYPE = "ENTERPRISE_RESELLER";
var RESELLER_SUBSCRIPTION_TYPE = "ENTERPRISE_RESELLER_MA";
var SUB_ACCOUNT_SUBSCRIPTION_TYPE = "ENTERPRISE_PRO+";

var ENTERPRISE_PORTAL_TITLE = "GigSky Enterprise Manager";
var ADMIN_PORTAL_TITLE = "Enterprise Admin Portal";
