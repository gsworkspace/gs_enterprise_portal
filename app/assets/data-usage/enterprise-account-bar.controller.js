'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('DuEntAccCtrl', EnterpriseAccount);

	EnterpriseAccount.$inject = ['$scope', 'requestGraphData','AccountService','dataUsageService','commonUtilityService'];

    /**
     @constructor EnterpriseAccount()
     @param {object} $scope - Angular.js $scope
     @param {object} requestGraphData - factory service
     */
    function EnterpriseAccount($scope, requestGraphData,AccountService,dataUsageService,commonUtilityService) {
        var enterpriseAccount = this;

		$scope.graphObj = {};

		/**
		 * Start up logic for controller
		 */
		function activate(){
			$scope.graphObj.requestData = requestData;
			$scope.graphObj.updateGraphOptions = addOrUpdateOptions;
			$scope.graphObj.getdata = getdata;

			console.log("Activation Init");
		}

        function addOrUpdateOptions(){

			console.log("AddOrUpdate Inv");

	        if($scope.graphObj.options)
		        delete $scope.graphObj.options;
	        
	    	$scope.graphObj.options = {
		    	title: {
	                enable: true,
	                text: 'Enterprise Usage by Day',
	                className: 'nvd3-gs-title', //Doesn't work with current NVD3.js
	                css: {
		               textAlign: "center",
		               textTransform: "uppercase",
		               fontFamily: "Montserrat",
		               marginTop: 15,
		               marginBottom: 0
	                }
	            },
	            subtitle: {
		            enable: true,
		            text: function() {
						return 'for billing period : ' + $scope.graphObj.month.getMonthName()+'-'+$scope.graphObj.month.getFullYear();
					},
							className: 'nvd3-gs-subtitle', //Doesn't work with current NVD3.js
		            css: {
			            textAlign: "center",
			            fontSize: 10,
			            lineHeight: 1
		            }
	            },
	            chart: {
	                type: 'historicalBarChart',
	                transitionDuration: 500,
	                height: (document.body.clientWidth > 650 ? 450 : document.body.clientWidth * (450/600)),
	                margin : {
	                    top: 25,
	                    right: ($(document).outerWidth() > 991 ? 70 : (document.body.clientWidth > 768 ? 60 : 25)),
	                    bottom: 50,
	                    left: 70
	                },
	                color: function (d, i) {
		                if(d.color)
		                	return d.color
		                return GRAPH_COLORS[1];
			        },
	                showLegend: false,
	                useInteractiveGuideline: true,
	                tooltips: true,
	                tooltip: {
		                gravity: "n", //Doesn't work with current NVD3.js
		                distance: 25
	                },
	                tooltipContent: function(key, y, e, graph) {
		                var date = d3.time.format('%b %d')(new Date(graph.series.values[graph.pointIndex].x));
						var tickUnit = commonUtilityService.getDataUsageUnit(graph.point.dataUsage);
						var tickVal = commonUtilityService.getUsageInUnit(graph.point.dataUsage,tickUnit);

			            return '<div class="nvd3-gs-usage">'+tickVal+'<span class="nvd3-gs-usage-unit">'+tickUnit+'</div><div class="nvd3-gs-date">'+date+'</div>';
			        },
			        clipEdge: true,
	                showValues: true,
	                x: function(d){return d.x},
	                xAxis: {
	                    axisLabel: enterpriseAccount.xaxisLabel, //This will need to be addressed post EFB Users Forum
	                    axisLabelDistance: 25,
	                    tickFormat: function(d) {	                    
	                        return d3.time.format('%d')(new Date(d));
	                    },
	                    tickValues: function (d) {
		                    var dateSets = [];
							var clientWidth = document.body.clientWidth;
							var bodyHasMenuPin = $('body').hasClass('menu-pin');
		                    if(clientWidth > 1200 || (clientWidth > 650 && !bodyHasMenuPin)){
					            for (var i = 0; i < d[0].values.length; i++) {
					                dateSets.push(d[0].values[i].x)
					            }
					        }
					        
					        else{
					            for (var i = 0; i < d[0].values.length; i++) {
						            if(i%3 == 0)
					               		dateSets.push(d[0].values[i].x);
					               	else 
					               		dateSets.push('');
					            }
					        }
					        
					        return dateSets;
				        },
				        height: 100,
				        tickPadding: 7,
	                    rotateLabels: 0,
	                    showMaxMin: false
	                },
	                y: function(d){return d.y},
	                yAxis: {
	                    axisLabel: enterpriseAccount.yaxisLabel,
	                    axisLabelDistance: 25,
	                    tickFormat: function(d){
	                        return d3.format(',g')(d);
	                    }
	                },
	                noData: "There is no data to display.",
					forceY : [0, 1]
	            },
	        };
        }

        function requestData(callback) {
			console.log("RequestData Inv");

            AccountService.getBillingCycle().success(function (data) {
				
                getdata(callback,data.billingStart,data.billingEnd);
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }
        function getdata(callback,startDay,endDay){

			if(enterpriseAccount.startDay != startDay || enterpriseAccount.endDay != endDay || !$scope.graphObj.data || $scope.graphObj.data.length<1) {
				requestGraphData.getDailyDataUsageOfEnterprise(startDay,endDay).success(function (data, status, headers, config) {
					enterpriseAccount.startDay = startDay;
					enterpriseAccount.endDay = endDay;
                    $scope.graphObj.data = dataUsageService.prepareDayWiseUsageData(data, startDay, endDay );
                    var startM = getDateObj(startDay).getMonthName();
                    var endM = getDateObj(endDay ).getMonthName();
                    enterpriseAccount.xaxisLabel = 'Days in '+ getDateObj(startDay).getMonthName() + ( startM != endM ? " - " + endM : "" );
					enterpriseAccount.yaxisLabel = $scope.graphObj.data[0].usageUnit +' Used';
					addOrUpdateOptions();
					prepareURL(config);
                    callback($scope.graphObj);

                }).error(function (data) {
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
			}else{

				callback( $scope.graphObj);
			}

        }
		function prepareURL(config){
			$scope.url = config.url+'&fileType=CSV&token='+encodeURIComponent(config.headers.token);
		}

		activate();
    }
})();