/* Controller */
(function () {

    'use strict';

    angular.module('app')
        .controller('PricingDetailsCtrl', PricingDetails);

    PricingDetails.$inject = ['$scope', '$q', 'SettingsService', 'commonUtilityService', 'loadingState','AuthenticationService','$rootScope','analytics'];

    /**
     @constructor PricingDetails()
     */
    function PricingDetails($scope, $q, SettingsService, commonUtilityService, loadingState, AuthenticationService,$rootScope,analytics){

        var pricingDetails = this;
        var enterpriseId = sessionStorage.getItem("accountId");
        pricingDetails.isZoneEdit = false;

        /**
         * @func activate()
         * @desc controller start up logic
         */
        function activate() {

            // $scope
            $scope.isRBACEnabled = RBAC_ENABLED;

            $scope.showErrMsg = function( errorMsg ) {commonUtilityService.showErrorNotification( errorMsg );};
            $scope.showSuccessMsg= function( msg ){commonUtilityService.showSuccessNotification( "Zone Nickname updated successfully!"  );};
            $scope.updateZone = updateZone;

            // Enterprise account
            pricingDetails.account = {};
            pricingDetails.account.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

            // Enterprise details
            pricingDetails.scrollToElem = function($event){
                if(!$("#"+$event.currentTarget.parentElement.id).inView()){
                    $event.currentTarget.parentElement.scrollIntoView(false);
                }

            };

            pricingDetails.preparePricingInfoTable = preparePricingInfoTable;

            // Initialize
            _pricingDetails();
            _isEditingZoneAllowed();

            // Loading Factory
            loadingState.hide();
        }

        activate();

        /**
         * @func _pricingDetails()
         * @desc controller request logic
         * @private
         */
        function _pricingDetails() {
            pricingDetails.errorMessage = '';
            var enterpriseQueryParam = {};
            enterpriseQueryParam.details = 'company';

            $q.all([SettingsService.getPlanDetails(), SettingsService.getEnterpriseDetails(enterpriseId,enterpriseQueryParam)]).then(function (response) {
                pricingDetails.zones = response[0].data.list;
                preparePricingInfoTable(pricingDetails.zones);
                pricingDetails.account.currency = commonUtilityService.getLocalCurrencySign(response[1].data.company.currency);
            },function (data) {
                pricingDetails.errorMessage = "Unable to load pricing data"
                commonUtilityService.showErrorNotification((data&&data.data)?(data.data.errorStr):null);
            });
        }

        /**
         * @func updateZone()
         * @param {number} ZoneId - zone ID
         * @param {string} nickName - zone nickname
         * @desc - updates a zones nickname
         * @returns {*} - Review return type
         */
        function updateZone(ZoneId,nickName){
            var zoneInfo = {
                "type" : "ZoneInfo",
                "nickName" : nickName
            };

            analytics.sendEvent({
                category: "Operations",
                action: "Update Zone Nickname",
                label: "Enterprise Settings"
            });

            return SettingsService.updateZoneDetail(zoneInfo,ZoneId);
        }

        function _isEditingZoneAllowed(){
            var permissionList = ['ACCOUNT_EDIT'];
            pricingDetails.isZoneEdit = AuthenticationService.isOperationAllowed(permissionList) && !($scope.isSubAccount);
        }

        $.fn.inView = function(){
            //Window Object
            var win = $(window);
            //Object to Check
            var obj = $(this);
            //the top Scroll Position in the page
            var scrollPosition = win.scrollTop();
            //the end of the visible area in the page, starting from the scroll position
            var visibleArea = win.scrollTop() + win.height();
            //the end of the object to check
            var objEndPos = (obj.offset().top + obj.outerHeight());
            return(visibleArea >= objEndPos && scrollPosition <= objEndPos ? true : false)
        };

        /**
         * @func preparePricingInfoTable
         * @desc prepare the pricing info table based on the pricing model version
         * @param zonesList
         */
        function preparePricingInfoTable(zonesList){
            if($rootScope.pricingModelVersion == '1.1'){
                return;
            }

            for(var i=0; i < zonesList.length; i++){
                var zone = zonesList[i];
                zone.buckets = [];

                if(!zone.bucketPricingInfo)
                    continue;

                zone.minimumCommitmentData = 0;
                zone.minimumCommitmentCost = zone.bucketPricingInfo.minimumCommitment;

                var remainingCommitmentCost = zone.bucketPricingInfo.minimumCommitment;

                var buckets =(zone.bucketPricingInfo)? zone.bucketPricingInfo.list:[];

                for(var j=0; j< buckets.length; j++){

                    var bucket = buckets[j];
                    var startRange = isNaN(bucket.startRange)?0:bucket.startRange;
                    var endRange = bucket.endRange;

                    bucket.tier = 'Tier '+(j+1);
                    bucket.range  = SettingsService.preparePricingRange(startRange,endRange,bucket.unitType);

                    if(remainingCommitmentCost == undefined || remainingCommitmentCost == 0){
                        bucket.committedData = 'NA';
                        zone.buckets.push(bucket);
                        continue;
                    }

                    if(endRange == null){
                        var committedData = (remainingCommitmentCost * bucket.unitValue) / bucket.pricePerUnit;
                        bucket.committedData = isNaN(committedData) ?0:committedData;
                        remainingCommitmentCost = 0;
                    }
                    else{

                       var dataInTier = (endRange - startRange);
                       var dataInBucket = dataInTier / bucket.unitValue ;
                       var dataCostInBucket = dataInBucket *  bucket.pricePerUnit;

                       if(dataCostInBucket <= remainingCommitmentCost ){
                           remainingCommitmentCost -= dataCostInBucket;
                           bucket.committedData = isNaN(dataInTier)?0:dataInTier;
                       }
                       else{
                           var committedData = (remainingCommitmentCost * bucket.unitValue) / bucket.pricePerUnit;
                           bucket.committedData = isNaN(committedData)?0:committedData;
                           remainingCommitmentCost = 0;
                       }

                    }

                    zone.minimumCommitmentData += commonUtilityService.getUsageInBytes(bucket.committedData,bucket.unitType);

                    bucket.committedData = (bucket.committedData != 0)?SettingsService.formatDataSize(commonUtilityService.getUsageInBytes(bucket.committedData,bucket.unitType)):0;

                    zone.buckets.push(bucket);
                }

                zone.minimumCommitmentData = (zone.minimumCommitmentData != 0)?SettingsService.formatDataSize(zone.minimumCommitmentData):0;

            }
        }

    }
})();