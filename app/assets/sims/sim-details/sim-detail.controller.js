'use strict';

(function () {
    angular.module('app')
        .controller('SIMDetailCtrl', SIMDetail);

    SIMDetail.$inject = ['$scope', '$stateParams','SIMService', 'commonUtilityService','AuthenticationService'];

    /**
     * @param $scope
     * @param SIMService
     * @param commonUtilityService
     * @param $stateParams
     * @constructor SIMDetail
     */
    function SIMDetail($scope, $stateParams, SIMService, commonUtilityService,AuthenticationService) {
        var SIMDetailVM = this;

        SIMDetailVM.getSIMDetails = getSIMDetails;

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            $scope.imsiModalObj = {};
            SIMDetailVM.simId = $stateParams.iccId;
            SIMDetailVM.accountId = sessionStorage.getItem("accountId");
            SIMDetailVM.connectionStatus = null;
            $scope.isSIMExpanded = false;
            getSIMDetails();
        }


        function setPreferredIMSIPriority(preferredIMSIsList, imsi) {
            for (var i in  preferredIMSIsList) {
                var preferredIMSI = preferredIMSIsList[i];
                if (imsi.profile == preferredIMSI.profile) {
                    imsi.priority = preferredIMSI.priority;
                    imsi.profilePriority = imsi.profile + " (Preferred : " + preferredIMSI.priority + ")";
                    break;
                }
            }

            imsi.profilePriority = (imsi.profilePriority) ? imsi.profilePriority : imsi.profile;
        }

        /**
         * @function getSIMDetails
         * @description fetches the SIM Details
         */
        function getSIMDetails() {
            var queryParam = {
                details:"account|addedToAccountOn|nickName|user"
            };

            var permissionList = ['SIM_READ_ADVANCED','SIM_READ_EXTENDED'];
            var fetchSimProfileDetails = AuthenticationService.isOperationAllowed(permissionList);

            queryParam.details = fetchSimProfileDetails ? queryParam.details + "|simProfileDetails" : queryParam.details;

            SIMService.getSimDetail(SIMDetailVM.accountId,SIMDetailVM.simId,queryParam).success(function(response){
                SIMDetailVM.SIM = response;
                SIMDetailVM.user = response.user ? response.user.firstName + " " + response.user.lastName : 'Unassigned';
                SIMDetailVM.group = response.account ? response.account.accountName : '';
                if(response.imsis && response.imsis.length > 0){
                    SIMDetailVM.imsis = response.imsis;
                    SIMDetailVM.isSingleImsi = (SIMDetailVM.imsis.length <= 1) ? true : false;
                    var preferredIMSIsList = response.preferredIMSIProfilesList;
                    for(var i in SIMDetailVM.imsis){
                        var imsi = SIMDetailVM.imsis[i];
                        if(imsi.activationStatus == 'ACTIVE'){
                            SIMDetailVM.imsiProfile  = imsi.profile;
                            SIMDetailVM.msisdn = imsi.msisdn;
                        }
                        setPreferredIMSIPriority(preferredIMSIsList, imsi);
                    }
                }

            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        activate();

    }
})();