'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('UpdateAssigneeCtrl', UpdateAssignee);

	UpdateAssignee.$inject = ['$scope', '$sce', 'modalDetails','notifications','UserService'];
	 
	/**
		@constructor UpdateAssignee()
		@param {object} $scope - Angular.js $scope
		@param {object} $sce - Angular.js strict contextual escaping service
		@param {object} modalDetails - Factory object
		@param {object} APIRequests - Factory object
		@param {object} notifications - Factory object
	*/   
	function UpdateAssignee($scope, $sce, modalDetails, notifications,UserService){
		
		var updateAssigneeModalObj = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			/* Form elements object */
			updateAssigneeModalObj.users = [{}];

			/* modalDetails factory */
			modalDetails.scope(updateAssigneeModalObj, true);
			modalDetails.attributes({
				id: 'updateAssigneeModal',
				formName: 'updateAssignee',
				header: 'Update Assignee',
				cancel: 'Cancel',
				submit: 'Assign SIM to User'
			});
			modalDetails.cancel(function(){  updateAssigneeModalObj.group = {}; delete updateAssigneeModalObj.users.user;
				delete $scope.updateAssigneeModalObj.updateUserErrMsg; });
			$scope.$parent.updateAssigneeModalObj.updateAssigneeModalSubmit = updateSubmit;
			$scope.$parent.updateAssigneeModalObj.updateAssigneeModalAttributes = updateModalAttributes;
			$scope.$parent.updateAssigneeModalObj.getGroupModalAssignee = getSelectedAssignee;
			updateAssigneeModalObj.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

			UserService.getUsers( { count:100000, startIndex:0, details:"fullName", sortBy:"fullName", sortDirection:"ASC"} ).success(function (response) {
				updateAssigneeModalObj.users = response.list;

				/*Adding fullName property to user object which helps to
				 search for both firstName and lastName in assignee modal.*/
				for(var i in updateAssigneeModalObj.users){
					var fName = updateAssigneeModalObj.users[i].firstName;
					var lName = updateAssigneeModalObj.users[i].lastName;
					var fullName = fName + " " + lName;

					updateAssigneeModalObj.users[i].fullName = fullName;
				}
			}).error(function(data,status){
				console.log('get users failure');
				console.log(data);
			});
		}

		/**
			@func updateModalAttributes()
			@param {object} obj - the object to pass to modalDetails factory
			@desc - Expose access to change the attributes of the modal for directive gs-verification-modal	
		*/
		function updateModalAttributes(obj){
			modalDetails.scope(updateAssigneeModalObj, false);
			modalDetails.attributes(obj);
		}				
		
		/**
			@func updateSubmit()
			@param {function} func - the function the execute on submit
			@desc - Expose access to change the submit function for directive gs-verification-modal.
			When the function is complete, remove the group data;
		*/
		function updateSubmit(rows,func, attrs){
			if (attrs)
				modalDetails.attributes(attrs);
				modalDetails.scope(updateAssigneeModalObj, false);

			modalDetails.submit(function () {

				if( typeof updateAssigneeModalObj.users.user == "undefined" || Object.getOwnPropertyNames(updateAssigneeModalObj.users.user).length == 0) {
					console.log("invalid user !!!");
					if( updateAssigneeModalObj.users.length > 0 ) $scope.updateAssigneeModalObj.updateUserErrMsg = "Please select an valid User!";
					return false;
				}
				else {
					func(updateAssigneeModalObj.users.user, rows);
					delete updateAssigneeModalObj.users.user;
					delete $scope.updateAssigneeModalObj.updateUserErrMsg;
					angular.element('#' + $scope.updateAssigneeModalObj.modal.id).modal('hide');

				}
			});
		}
		
		/**
			@func getSelectedAssignee()
			@return - the userId key value from updateAssigneeModalObj.user (updateAssigneeModalObj.user.userId)
			@desc - Expose access to the updateAssigneeModalObj.user.userId value for directive gs-verification-modal	
		*/
		function getSelectedAssignee(){
			return updateAssigneeModalObj.users.user;
		}

		activate();

	}
})();
