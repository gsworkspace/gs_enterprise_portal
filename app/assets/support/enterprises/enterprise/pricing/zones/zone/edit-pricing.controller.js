/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('PricingInfoCtrl', PricingInfo);

    PricingInfo.$inject=['$scope','$rootScope','commonUtilityService','SettingsService','loadingState','$q','$stateParams','AuthenticationService','zonePricingService','$sce'];

    /**
     * @constructor PricingInfo()
     * @param $scope
     * @param $rootScope
     * @param commonUtilityService
     * @param SettingsService
     * @param loadingState
     * @param $q
     * @param $stateParams
     * @param AuthenticationService
     * @param zonePricingService
     * @param $sce
     * @constructor
     */
    function PricingInfo($scope,$rootScope,commonUtilityService,SettingsService,loadingState,$q,$stateParams,AuthenticationService,zonePricingService,$sce){

        var pricingInfo = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;
            $scope.zoneId = $stateParams.zoneId;
            $scope.$parent.editPricingObj = $scope.$parent.editPricingObj ? $scope.$parent.editPricingObj : {};
            $scope.$parent.editPricingObj.updatePricing = updatePricing;

            $scope.unitsAvailable = ['MB','GB','TB'];
            $scope.unitMap = {Bytes: 1, KB:2, MB:3, GB:4, TB:5};
            pricingInfo.unitType = 'TB';

            pricingInfo.onUnitChange = onUnitChange;
            pricingInfo.savePricing = savePricing;
            pricingInfo.validateInput = validateInput;
            pricingInfo.cancel = function () {
                $scope.$parent.editPricingObj.editMode = false;
            };

        }
        activate();

        pricingInfo.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};


        /**
         * @desc Adds a bucket(Tier)
         * @param bucketList - list of buckets
         * @param index - index of the bucket where the new bucket is added after it
         *
         * the new bucket will be added at the (index + 1)th position
         */
        $scope.addBucket = function(bucketList,index){
            $scope.errors = zonePricingService.validatePricingInfo(pricingInfo.zone, true);
            return $scope.errors.hasErrors ? null : zonePricingService.addBucket(bucketList,index);
        };

        /**
         * @desc Removes a bucket(Tier)
         * @param bucketList - list of buckets
         * @param index - index of the bucket which will be removed
         *
         * the bucket will be removed at (index)th position
         */
        $scope.removeBucket = function(bucketList,index){
            $scope.errors = null;

            index?zonePricingService.removeBucket(bucketList,index):commonUtilityService.showErrorNotification('Cannot remove the bucket.');

        };

        /**
         * @desc updates the pricing form based on the zone details provided
         * @param zone - zone details
         * @param currency - account's currency
         *
         * Updates the pricing form.
         */
        function updatePricing(zone, currency) {
            pricingInfo.errorMessage = null;
            $scope.errors = null;
            pricingInfo.currency = currency;
            var newZone = { type: 'ZoneInfo', bucketPricingInfo: {list:[ { unitType:'MB', unitValue:null, pricePerUnit:null, startRange:0, endRange:null}]}};
            pricingInfo.unitType = zone ? 'TB' : 'MB';


            if(zone){
                var oldZone = angular.copy(zone);
                if(oldZone.bucketPricingInfo){
                    newZone.bucketPricingInfo = cloneBucketPricingInfo(oldZone.bucketPricingInfo);
                    pricingInfo.unitType = getZoneUnitType(newZone.bucketPricingInfo.list, pricingInfo.unitType);
                    convertBucketsIntoUnit(newZone, pricingInfo.unitType);
                }
            }

            pricingInfo.zone =  newZone;
            pricingInfo.previousZoneData = angular.copy(pricingInfo.zone);
            $scope.pricingBuckets = pricingInfo.zone.bucketPricingInfo.list;
        }

        /**
         * @desc clones the bucket pricing information
         * @param bucketPricingInfo - bucket pricing info of a zone
         * @returns {Object} - cloned bucketPricingInfo
         */
        function cloneBucketPricingInfo(bucketPricingInfo) {
            var newBucketPricingInfo = {list:[]};

            if(bucketPricingInfo.minimumCommitment){
                newBucketPricingInfo.minimumCommitment = bucketPricingInfo.minimumCommitment;
            }

            bucketPricingInfo.list = bucketPricingInfo.list ? bucketPricingInfo.list : [];

            for(var i=0; i<bucketPricingInfo.list.length; i++){
                var bucket = bucketPricingInfo.list[i];
                var newBucket = {startRange: bucket.startRange, endRange: bucket.endRange, unitType: bucket.unitType, unitValue: bucket.unitValue, pricePerUnit: bucket.pricePerUnit};
                newBucketPricingInfo.list.push(newBucket);
            }

            return newBucketPricingInfo;

        }

        /**
         * @desc Gets the suitable unit type for the zone
         * @param buckets
         * @param unitType
         * @returns {*} - appropriate unit type
         */
        function getZoneUnitType(buckets, unitType){
            for(var i=0; i<buckets.length; i++){
                unitType = getSmallerUnitType(buckets[i].unitType, unitType);
            }

            return unitType;
        }

        /**
         * @desc returns the smaller unit type of the given unit types
         * @param curUnitType
         * @param prevUnitType
         * @returns {*} - smallest unit type among the curUnitType & prevUnitType
         */
        function getSmallerUnitType(curUnitType, prevUnitType) {

            if($scope.unitMap[curUnitType]){
                if($scope.unitMap[curUnitType] < $scope.unitMap.MB){
                    commonUtilityService.showErrorNotification('Cannot edit the pricing as it contains Tier(s) with unit type lesser than MB.');
                    return pricingInfo.cancel();
                }
                return $scope.unitMap[curUnitType] < $scope.unitMap[prevUnitType] ? curUnitType : prevUnitType;
            }
            return prevUnitType;
        }

        /**
         * @desc converts buckets into the mentioned unit type
         * @param zone - zone details
         * @param unitType
         */
        function convertBucketsIntoUnit(zone, unitType, precision){
            var bucketInfo = zone.bucketPricingInfo;
            for(var i=0; i< bucketInfo.list.length; i++){
                convertBucketIntoUnit(bucketInfo.list[i], unitType, precision);
            }
        }

        /**
         * @desc converts the bucket into the mentioned unit type
         * @param bucket - bucket details
         * @param unitType
         */
        function convertBucketIntoUnit(bucket, unitType, precision) {
            bucket.startRange = bucket.startRange ? formatDataSize(bucket.startRange, bucket.unitType, unitType, precision) : bucket.startRange;
            bucket.endRange = bucket.endRange ? formatDataSize(bucket.endRange, bucket.unitType, unitType, precision) : bucket.endRange;
            bucket.unitValue = bucket.unitValue ? formatDataSize(bucket.unitValue, bucket.unitType, unitType, precision) : bucket.unitValue;
            bucket.unitType = unitType;
        }


        /**
         * @desc converts the given usage into usage in 'newUnitType'
         * @param dataUsage - usage in oldUnitType
         * @param oldUnitType - current unit type
         * @param newUnitType - new unit type
         */
        function formatDataSize(dataUsage, oldUnitType, newUnitType, precision) {
            var dataUsageInBytes = commonUtilityService.getUsageInBytes(dataUsage,oldUnitType);
            return commonUtilityService.getUsageInUnit(dataUsageInBytes, newUnitType, precision);
        }

        /**
         * @desc Converts the zone into the new unit when unit type is changed
         * @param unit - new unit type
         */
        function onUnitChange(unit) {
            $scope.errors = null;
            var zone = angular.copy(pricingInfo.zone);
            var bucketUnitType = zone.bucketPricingInfo.list.length ? zone.bucketPricingInfo.list[0].unitType : unit;
            convertBucketsIntoUnit(zone, unit, 10);

            var allowed = isPricingValidationAllowed(zone);
            if(allowed){
                var errors = zonePricingService.validatePricingInfo(zone, true);
                if(errors.hasErrors){
                    pricingInfo.unitType = bucketUnitType;
                    commonUtilityService.showErrorNotification('Cannot change the unit to '+unit+' as it results into Tier(s) with decimal value.');
                    return;
                }
            }

            pricingInfo.zone = zone;
            $scope.pricingBuckets = pricingInfo.zone.bucketPricingInfo.list;

        }

        /**
         * @desc checks the condition whether to validate pricing info or not. [Condition - don't validate buckets if the bucket has single tier & bucket size is empty]
         * @param zone
         * @returns {boolean}
         */
        function isPricingValidationAllowed(zone) {
            var bucketList = zone.bucketPricingInfo.list;

            if(!bucketList.length || bucketList.length>1){
                return true;
            }

            return !!(bucketList[0].unitValue );

        }

        /**
         * @desc validates the input value of type 'number' & doesn't allow to enter '-' & 'e'
         * @param event
         */
        function validateInput(event){

            var notAllowedCharsRegEx =/[-e]/;

            if(notAllowedCharsRegEx.test(event.key))
            {
                event.preventDefault();
            }

        }

        /**
         * @desc saves the pricing info changes
         * @param zone - zone details
         */
        function savePricing(zone){

            if(pricingInfo.previousZoneData && zone && angular.equals(pricingInfo.previousZoneData,zone)){
                pricingInfo.errorMessage = "There is no change in the data to update.";
                return;
            }
            pricingInfo.errorMessage = null;

            $scope.errors = zonePricingService.validatePricingInfo(zone);

            if($scope.errors && !$scope.errors.hasErrors){
                var zoneInfo = prepareZoneUnit(zone);
                zonePricingService.updatePricing($scope.enterpriseId, $scope.zoneId, zoneInfo).success(function () {
                    $scope.$parent.editPricingObj.editMode = false;
                    $scope.$parent.editPricingObj.onSuccessCallback();
                }).error(function (error) {
                    commonUtilityService.showErrorNotification(error.errorStr);
                })
            }

        }

        /**
         * @desc prepares the appropriate unit type for each bucket in the zone
         * @param zone - zone details
         * @returns {*} - zone details in the appropriate unit type for each bucket
         */
        function prepareZoneUnit(zone) {
            var zoneCopy = angular.copy(zone);

            var buckets = zoneCopy.bucketPricingInfo.list;
            var newBuckets = [];

            for(var i=0; i<buckets.length; i++){
                newBuckets.push(prepareBucketUnit(buckets[i]));
            }

            zoneCopy.bucketPricingInfo.list = newBuckets;

            return zoneCopy;
        }

        /**
         * desc prepares the appropriate unit type for the bucket
         * @param bucket - bucket details
         * @returns {*} - bucket details in the appropriate unit type
         */
        function prepareBucketUnit(bucket) {

            var finalUnit = null;

            var bucketSizeUnit = (bucket.unitValue)? commonUtilityService.getDataUsageUnit(commonUtilityService.getUsageInBytes(bucket.unitValue, bucket.unitType)): bucket.unitType;
            var startRangeUnit = (bucket.startRange)? commonUtilityService.getDataUsageUnit(commonUtilityService.getUsageInBytes(bucket.startRange, bucket.unitType)): bucket.unitType;

            if(bucket.startRange ===0){
                finalUnit = bucketSizeUnit;
            }else if(bucket.startRange){
                finalUnit = $scope.unitMap[startRangeUnit] < $scope.unitMap[bucketSizeUnit] ?  startRangeUnit : bucketSizeUnit;
            }

            if(bucket.unitType !== finalUnit){
                var bucketCopy = angular.copy(bucket);
                convertBucketIntoUnit(bucketCopy, finalUnit, 10);
                var errors = zonePricingService.validateBucket(bucketCopy, {}, 0, 1);

                return errors.hasErrors ? bucket : bucketCopy;
            }

            return bucket;
        }

    }
})();
