'use strict';

(function(){
    angular
        .module('app')
        .factory('simSessionService', SimSessionService);

    SimSessionService.$inject = ["commonUtilityService"];

    /**
     @function AlertsService()
     */
    function SimSessionService(commonUtilityService) {

        return {
            getSimUpdateSession: getSimUpdateSession,
            getDisplayStatusOfSIMAssociation:getDisplayStatusOfSIMAssociation,
            getSimAddUpdateSession:getSimAddUpdateSession
        };

        function getSimUpdateSession(enterpriseId, queryParams) {

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + enterpriseId + '/sims/upload/session';
            var headerAdd = ['PROVISIONING_SIMS_GET_ADD_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'GET', headerAdd, queryParams);
        }
        function getDisplayStatusOfSIMAssociation(status, simCountObj){

            status = (status == 'ERROR' && simCountObj && simCountObj.COMPLETED)?'PARTIAL':status;

            return getStatusClassMap(status);

        }
        function getSimAddUpdateSession(enterpriseId,statusId, queryParams) {
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + enterpriseId + '/sims/upload/session/' + statusId;
            var headerAdd = ['PROVISIONING_SIMS_GET_ADD_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd, queryParams);
        }

    }
 })();