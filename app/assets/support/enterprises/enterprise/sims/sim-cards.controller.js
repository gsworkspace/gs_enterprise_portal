/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('SIMCardsCtrl', SIMCards);

    SIMCards.$inject=['$scope', '$sce', '$compile', 'dataTableConfigService','SIMService','commonUtilityService','loadingState','$stateParams','$sanitize','SettingsService','simsCommonFactory','$location','AuthenticationService','$rootScope','$q'];

    /**
     * @constructor SIMCards()
     * @param $scope
     * @param $sce
     * @param $compile
     * @param dataTableConfigService
     * @param SIMService
     * @param commonUtilityService
     * @param loadingState
     * @param $stateParams
     * @param $sanitize
     * @param SettingsService
     * @param SimsCommonFactory
     * @param $location
     * @param authenticationService
     * @param $rootScope
     * @constructor
     */
    function SIMCards($scope, $sce, $compile, dataTableConfigService, SIMService, commonUtilityService, loadingState, $stateParams,$sanitize,SettingsService,SimsCommonFactory,$location,authenticationService,$rootScope,$q){

        var SIMCardsVm = this;
        var uid;
        var filterValue;
        var statusMap = {INACTIVE:'inactive',ACTIVE:'active',BLOCKED:'blocked',UNBLOCKED:'unblocked'};
        $scope.entAssociateSIMModalObj = {};
        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_simCardsDataTable_']);
            // Controller As
            SIMCardsVm.trustAsHtml = function (value){return $sce.trustAsHtml(value);};
            SIMCardsVm.bulkAction = bulkAction;
            $scope.emptyTableMessage = "No SIMs available.";
            $scope.alertEnabled = false;
            SIMCardsVm.bulkSimAssociation = bulkSimAssociationModal;

            $scope.enterpriseId = $stateParams.accountId;
            $scope.initialDataAvailable = false;

            function bulkSimAssociationModal() {
                if ($scope.isSubAccount) {
                    commonUtilityService.showErrorNotification('Adding SIMs is not allowed to this enterprise. Please add SIMs to parent reseller account, who can then assign SIMs to this enterprise.');
                } else {
                    $scope.dataTableObj.bulkUpload('bulkUploadSimsModal');
                    $scope.entAssociateSIMModalObj.updateEntAssociationSIMModal();
                }
            }

            // Prepare simsCommonFactory with controller scope
            SimsCommonFactory.setScope($scope);
            $scope.zoneStatusModalObj = {};

            var userData = JSON.parse(sessionStorage.getItem("gs-showSimsAssignedToCurrentGapUser"));
            if(userData){
                filterValue = userData.firstName;
                uid = userData.userId;
                SIMCardsVm.searchValue = filterValue;
            }else{
                uid = null;
            }


            // DataTable
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {"value": "", "DbValue":""},
                {"value": "ICCID", "DbValue":"simId"},
                {"value": "Status", "DbValue":"simStatus"},
                {"value": "Zone(s) Access", "DbValue":""},
                {"value": "Allocation", "DbValue":"limitKB"},
                {"value": "Nickname","DbValue":"nickName"},
                {"value": "Assigned User","DbValue":"firstName"},
                {"value": "Group","DbValue":"accountName"},
                {"value": "Usage", "DbValue":"datausage"}
            ];
            $scope.dataTableObj.tableID = "simCardsDataTable";

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
        }

        activate();

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         * @private
         */
        function _setDataTableOptions(){
            return {
                order: [[ 7, "desc" ]],
                search: {
                    search: SIMCardsVm.searchValue
                },
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.simId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:true},
                    { orderable:true},
                    { orderable:false},
                    { orderable:true},
                    { orderable:true},
                    { orderable:true},
                    { orderable:true},
                    { orderable:true}
                ],
                columnDefs: [{
                    "targets": 0,
                    className: "checkbox-table-cell",
                    "render": function ( data, type, row, meta) {
                        return '<div gs-has-permission="PROVISIONING_SIMS_BLOCK_UNBLOCK,SIM_ACTIVATION" class="checkbox check-success large no-label-text hideInReadOnlyMode"><input type="checkbox" value="1" id="checkbox_'+row.simId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.simId+'"></label></div>';
                    }
                },{
                    targets: 1,
                    render: function (data, type, row) {
                        return '<a ui-sref="support.sim.simDetail({accountId:\''+$scope.enterpriseId+'\',iccId:\''+row.simId+'\'})" class="cursor">'+row.simId+'</a>';
                    }
                }, {
                    targets: 2,
                    render:function (data, type, row){
                        return '<div class="status-wrapper"><span class="status '+( row.status  == "ACTIVE" ? "active" : "inactive" )+'"></span><span class="status-text">'+( statusMap[row.status] ) +'</span></div>';
                    }
                }, {
                    targets: 3,
                    render:function( data, type, row, meta) {
                        var zoneStatus = SimsCommonFactory.getZoneStatusForSim(row.zoneConnectivityStatus,SIMCardsVm.zonesCount);
                        return ($scope.alertEnabled) ? '<div class="status-wrapper status-as-link cursor" ng-click="SIMCardsVm.zoneStatus('+ meta.row +')"><span class="status '+( zoneStatus  == "Full Access" ? "full-access" : (zoneStatus  == "Partial Access" ? "partial-access" : "no-access") )+'"></span><span class="status-text">'+zoneStatus+'</span></div>' : '';
                    }
                },{
                    "targets": 4,
                    "render": function (data, type, row) {
                        $scope.simList[ row.simId ] =  row;
                        return  ($scope.alertEnabled) ? '' : getSimCreditLimit(row);

                    },
                    "defaultContent": '350'

                }, {
                    targets: 5,
                    render: function (data, type, row) {
                        return '<span>'+row.nickName+'</span>';
                    }
                }, {
                    targets: 6,
                    render: function (data, type, row) {
                        if(row.user){
                            var firstName = $sanitize(row.user.firstName);
                            var lastName = $sanitize(row.user.lastName);
                        }

                        return( !$.isEmptyObject(row.user) ? "<span class='nowrap'>"+firstName+' '+lastName+"<span>":'Unassigned');
                    },
                    "defaultContent": 'Unassigned'
                },{
                    "targets": 7,
                    "render": function (data, type, row) {
                        var accountName = $sanitize(row.account.accountName);
                        return( accountName ? "<span class='nowrap'>"+accountName+"</span>" : 'Default');
                    },
                    "defaultContent": 'Default'
                }, {
                    targets: 8,
                    render:function( data, type, row) {
                        return  '<span>'+((row.dataUsedInBytes!==undefined)?row.dataUsedInBytes === +row.dataUsedInBytes && row.dataUsedInBytes !== (row.dataUsedInBytes|0)? commonUtilityService.getUsageInMB()(row.dataUsedInBytes) : commonUtilityService.getUsageInMB()(row.dataUsedInBytes): 0)+'</span>';
                    }
                }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    if(aoData){
                        SIMCardsVm.searchValue =  (aoData[5])?aoData[5].value.value:'';
                    }

                    var simsQueryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    if(filterValue && filterValue != SIMCardsVm.searchValue){
                        uid = null;
                        sessionStorage.removeItem("gs-showSimsAssignedToCurrentGapUser");
                    }

                    var getSIMs;
                    if(uid){
                        simsQueryParam.search = '';
                        getSIMs = SIMService.getSIMsByUserId(uid,simsQueryParam);
                    }else{
                        getSIMs = SIMService.getSIMs(simsQueryParam,$scope.enterpriseId);
                    }

                    var promiseArray = [getSIMs];


                    if(!$scope.initialDataAvailable){
                        var entQueryParam = {};
                        entQueryParam.details = 'accountName|alertVersionSupported';
                        var planQueryDetails = 'name';

                        promiseArray.push(authenticationService.getEnterpriseAccountDetails($scope.enterpriseId,entQueryParam),SettingsService.getPlanDetails(planQueryDetails,$scope.enterpriseId));
                    }

                    $q.all(promiseArray).then(function (response) {

                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response[0].data,oSettings,$scope.emptyTableMessage);
                        $scope.simList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.simList.length == 0);

                        if(!$scope.initialDataAvailable){
                            $scope.initialDataAvailable = true;

                            $rootScope.currentAccountName = response[1].data.accountName;
                            var alertVersion = response[1].data.alertVersionSupported;
                            $scope.alertEnabled = (alertVersion == "1.1");

                            SIMCardsVm.zonesCount = response[2].data.count;

                        }

                        (oSettings != null)?fnCallback(datTabData):loadingState.hide();

                    }, function (error) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.simList ||  $scope.simList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"SIMs.");
                        commonUtilityService.showErrorNotification(error.data.errorStr);
                    });


                },"fnDrawCallback": function( oSettings ) {

                    if($scope.simList)
                        dataTableConfigService.disableTableHeaderOps( $scope.simList.length, oSettings );

                        hideTableColumns();

                }
            };
        }

        function bulkAction(rows,modelKey) {

            return SIMService.bulkSIMactions(rows,$scope,modelKey, $scope.enterpriseId,true);

        }

        function hideTableColumns() {
            var $table = $("#" + $scope.dataTableObj.tableID);

            $scope.enableCheckbox = authenticationService.isOperationAllowed(['PROVISIONING_SIMS_BLOCK_UNBLOCK','SIM_ACTIVATION']);

            $table.DataTable().column(0).visible($scope.enableCheckbox);

            if (!$scope.alertEnabled) {
                $table.DataTable().column(3).visible(false);
            } else {
                $table.DataTable().column(4).visible(false);
            }

            $table.DataTable().columns(7).visible(!$scope.resellerOrSubAccount);
        }

        $scope.checkBoxClicked = function(event){
            $scope.dataTableObj.selectRow(event);
        };

        SIMCardsVm.zoneStatus = function(index){
            var sim = $scope.simList[index];
            SimsCommonFactory.showZoneStatusModal(sim,SIMCardsVm.zonesCount,$scope.enterpriseId);
        };

        function getSimCreditLimit( sim ){

            if(sim.creditLimitList && sim.creditLimitList.list  && sim.creditLimitList.list[0].limitKB !== undefined ) {
                return commonUtilityService.getUsageInMB()( sim.creditLimitList.list[0].limitKB * 1024 );
            }
            return commonUtilityService.getUsageInMB()(sessionStorage.getItem( "defaultCreditLimitForSims") * 1024 );
        }

    }
})();
