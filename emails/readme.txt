EMAILS README:
LAST MODIFIED DATE - November 9th 2015
--------------------------------------

READ THE FOLLOWING DESCRIPTION OF EMAILS 
TO UNDERSTAND HOW THE PROCESS WORKS:

email-raw.html:
This is the current production email without any inline CSS.

email-dev.html:
This is the current development email without any inline CSS.

email-stag.html:
This is the current staging email WITH inline CSS.
Please use this tool (http://templates.mailchimp.com/resources/inline-css/) to add CSS inline as it does it for you :)
Remember that you should copy email-dev.html, use the tool on the last line, and paste the output into email-stag.html

email-prod.html:
This is the current production email WITH inline CSS.
The contents from email-stag.html should only be copied over to this file once the email template has been tested using Litmus (litmus.com)