describe('EnterpriseAlertsCtrl', function () {

    var _commonUtilityService,_alertsService,_alertsCommonFactory;
    var alert = {
        "type": "AlertConfiguration",
        "accountId": 2,
        "accountName": "Uber",
        "alertType": "ACCOUNT_CUMULATIVE",
        "triggerCount": 311,
        "alertSpecification": {
            "type": "AlertSpecification",
            "enable": false,
            "limitValue": 20.00,
            "limitType": "COST",
            "actions": ["EMAIL"]
        },
        "updatedTime": "2016-06-21 05:04:27"
    };
    var MockAlertsResponse = {
        "type": "AlertConfigurationList",
        "count": 2,
        "totalCount": 2,
        "list": [{
            "type": "AlertConfiguration",
            "alertId": 1,
            "accountId": 2,
            "accountName": "Uber",
            "alertType": "ACCOUNT_CUMULATIVE",
            "triggerCount": 311,
            "alertSpecification": {
                "type": "AlertSpecification",
                "enable": false,
                "limitValue": 20.00,
                "limitType": "COST",
                "actions": ["EMAIL"]
            },
            "updatedTime": "2016-06-21 05:04:27"
        }, {
            "type": "AlertConfiguration",
            "alertId": 4,
            "accountId": 2,
            "accountName": "Uber",
            "alertType": "ACCOUNT_CUMULATIVE",
            "triggerCount": 311,
            "alertSpecification": {
                "type": "AlertSpecification",
                "enable": false,
                "limitValue": 4.00,
                "limitType": "COST",
                "actions": ["EMAIL"]
            }
        }]
    }

    beforeEach(module('app'));

    var enterpriseSpendAlertsCtrl;
    var scope = {};
    scope.enterpriseSpendAlerts = {};
    scope.alertModalObj = {};
    scope.$on= jasmine.createSpy('refreshDataTable spy method');
    beforeEach(inject(function ($injector, $controller, $compile, $timeout, dataTableConfigService, alertsService, commonUtilityService, alertsCommonFactory) {
        enterpriseSpendAlertsCtrl = $controller("EnterpriseSpendAlertsCtrl", {
            $scope: scope,
            $compile: $compile,
            $timeout: $timeout,
            alertsService: alertsService,
            dataTableConfigService: dataTableConfigService,
            commonUtilityService: commonUtilityService,
            alertsCommonFactory: alertsCommonFactory
        });

        _commonUtilityService = commonUtilityService;
        _alertsService = alertsService;
        _alertsCommonFactory = alertsCommonFactory;
        scope.alertModalObj.updateAlertModal = jasmine.createSpy('updateAlertModal spy method');
        scope.alertModalObj.setShowSpend = jasmine.createSpy('setShowSpend spy method');
        scope.alertModalObj.setShowZones = jasmine.createSpy('setShowZones spy method');

        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        spyOn(_alertsCommonFactory, 'showError');

    }));

    describe('Add alert', function () {

        it('should show success message', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Alert has been added');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();

        });

        it('should show appropriate error message on failure', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });

    });


    describe('Edit alert', function () {
        scope.enterpriseSpendAlertsList = MockAlertsResponse.list;


        it('modal should have Edit alert title message',function(){
            //Launch edit user modal
            scope.enterpriseSpendAlerts.showAlertModal = function(config, editData){
                expect(editData).not.toEqual(null);
                expect(config.isNew).toBe(false);
                expect(config.isSpendAlert).toBe(true);
                expect(config.isZoneAlert).toBe(false);
                expect(config.isGroupAlert).toBe(false);
            };
            scope.showEditAlertModal(0);
        });

        it('should show success message', function () {
            scope.showEditAlertModal(0);

            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);

            scope.editAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Alert has been updated');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        });

        it('should show appropriate error message on failure', function () {
            scope.showEditAlertModal(0);
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);
            scope.editAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });


    });

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.alertId);
    });

    it('Should test data-table columnDefs 1(when limitType is DATA)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.limitType = 'DATA';
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain(row.alertSpecification.limitValue);
    });

    it('Should test data-table columnDefs 1(when limitType is COST)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        sessionStorage.setItem( "gs-enterpriseCurrency","USD");
        alert.alertSpecification.limitType = 'COST';
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain('$'+row.alertSpecification.limitValue);
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain(row.alertSpecification.actions[0]);
    });

    it('Should test data-table columnDefs 3(when triggered count is 0)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.triggerCount = 0;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain('NO');
    });

    it('Should test data-table columnDefs 3(when triggered count is greater then 0)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.triggerCount = 300;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain('YES');
    });

    it('Should test data-table columnDefs 4(when alert status is enabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = true;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain('Enabled');
    });

    it('Should test data-table columnDefs 4(when alert status is disabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = false;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain('Disabled');
    });

    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain(row.alertId);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain('Edit');
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            }
        };
        spyOn(_alertsService, 'getAlerts').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.enterpriseSpendAlertsList.length).toBe(MockAlertsResponse.list.length);
    });

});