describe('GroupPerZonePerSIMAlertsCtrl', function () {

    var _commonUtilityService,_alertsService,_alertsCommonFactory;
    var alert = {
        "type": "AlertConfiguration",
        "alertId": 4,
        "accountId": 2,
        "accountName": "Uber",
        "alertType": "PER_SIM_PER_ZONE",
        "zoneId": 1,
        "zoneName": "2-Zone1",
        "zoneNickName": "2-Zone1",
        "alertHistoryActionStatus": "COMPLETED",
        "triggerCount": 1,
        "alertSpecification": {
            "type": "AlertSpecification",
            "enable": false,
            "limitValue": 1234.00,
            "limitType": "DATA",
            "actions": ["DISABLE"]
        }
    };
    var MockAlertsResponse = {
        "type": "AlertConfigurationList",
        "count": 1,
        "totalCount": 1,
        "list": [alert]
    };

    beforeEach(module('app'));

    var groupZonePerSIMAlertsCtrl;
    var scope = {};
    scope.groupZonePerSIMAlerts = {};
    scope.$on= jasmine.createSpy('refreshDataTable spy method');
    beforeEach(inject(function ($injector, $controller, $compile, $timeout, dataTableConfigService, alertsService, commonUtilityService, alertsCommonFactory,SettingsService) {

        var expectedEnterpriseResponse = {"type":"Account","accountId":4,"accountName":"UberCalifornia"};
        var _mockEnterprisePromise = {
            success: function(fn) {
                fn(expectedEnterpriseResponse);
                return _mockEnterprisePromise;
            },
            error: function(fn) {
                fn(expectedEnterpriseResponse);
                return _mockEnterprisePromise;
            }
        };
        spyOn(SettingsService, 'getEnterpriseDetails').and.returnValue(_mockEnterprisePromise);

        groupZonePerSIMAlertsCtrl = $controller("GroupZonePerSIMAlertsCtrl", {
            $scope: scope,
            $compile: $compile,
            $timeout: $timeout,
            alertsService: alertsService,
            dataTableConfigService: dataTableConfigService,
            commonUtilityService: commonUtilityService,
            alertsCommonFactory: alertsCommonFactory
        });

        expect(scope.accountName).toBe('UberCalifornia');
        _commonUtilityService = commonUtilityService;
        _alertsService = alertsService;
        _alertsCommonFactory = alertsCommonFactory;

        scope.alertModalObj.updateAlertModal = jasmine.createSpy('updateAlertModal spy method');
        scope.alertModalObj.setShowSpend = jasmine.createSpy('setShowSpend spy method');
        scope.alertModalObj.setShowZones = jasmine.createSpy('setShowZones spy method');

        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        spyOn(_alertsCommonFactory, 'showError');
        spyOn(_alertsCommonFactory, 'setAlertCustomized');

    }));

    describe('Add alert', function () {

        it('should show success message', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };

            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been added');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        });

        it('should show appropriate error message on failure', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });

    });


    describe('Edit alert', function () {
        scope.groupPerZonePerSIMAlertsList = MockAlertsResponse.list;

        it('modal should have Edit alert title message',function(){
            //Launch edit user modal
            scope.groupZonePerSIMAlerts.showAlertModal = function(alertType,isNew, isSpendAlert, isZoneAlert, isGroupAlert, editData){
                expect(editData).not.toEqual(null);
                expect(config.isNew).toBe(false);
                expect(config.isSpendAlert).toBe(false);
                expect(config.isZoneAlert).toBe(true);
                expect(config.isGroupAlert).toBe(false);
                expect(config.alertType).toBe('PER_SIM_PER_ZONE');

                scope.onAddCbk = scope.editAlert;
            };

            scope.showEditAlertModal(0);
        });

        it('should show success message', function () {
            scope.showEditAlertModal(0);

            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);

            scope.editAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been updated');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        });

        it('should show appropriate error message on failure', function () {
            scope.showEditAlertModal(0);
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);
            scope.editAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });

    });

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:0
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row)).toContain(row.alertId);
    });

    it('Should test data-table columnDefs 1(when zonenick name is not there)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.zoneNickName = '';
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe(row.zoneName);
    });

    it('Should test data-table columnDefs 1(when zonenick name exist)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.zoneNickName = '2-Zone1NickName';
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe(row.zoneNickName);
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row)).toBe(row.alertSpecification.limitValue);
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row)).toContain("Deactivate");
    });

    it('Should test data-table columnDefs 4',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row)).toBe(row.triggerCount);
    });

    it('Should test data-table columnDefs 5(when alert status is enabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = true;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row)).toContain('Enabled');
    });

    it('Should test data-table columnDefs 5(when alert status is disabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = false;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row)).toContain('Disabled');
    });

    it('Should test data-table columnDefs 6',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toContain(row.alertId);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toContain('Edit');
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            }
        };
        spyOn(_alertsService, 'getAlerts').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.groupPerZonePerSIMAlertsList.length).toBe(MockAlertsResponse.list.length);
        expect(_alertsCommonFactory.setAlertCustomized).toHaveBeenCalledWith(scope.groupPerZonePerSIMAlertsList);
    });

    it('Should test fnDrawCallback',function(){
        var oSettings = {};
        spyOn(_commonUtilityService, 'preventDataTableActivePageNoClick');
        scope.dataTableObj.dataTableOptions.fnDrawCallback(oSettings);

        expect(_commonUtilityService.preventDataTableActivePageNoClick).toHaveBeenCalled();
    });

});