describe('Add enterprise controller', function () {

    var  addEnterpriseCtrl,scope,_$sce, _modalDetails,_formMessage,_$q,_commonUtilityService,_$controller,_rootScope;

    var countries = [{"_id":"561771933b27e844933060b9","alpha-2":"AF","name":"Afghanistan","countryId":"561771933b27e844933060b9"},{"_id":"561771933b27e844933060ba","name":"Åland Islands","alpha-2":"AX","countryId":"561771933b27e844933060ba"},{"_id":"561771933b27e844933060bb","name":"Albania","alpha-2":"AL","countryId":"561771933b27e844933060bb"},{"_id":"561771933b27e844933060bc","name":"Algeria","alpha-2":"DZ","countryId":"561771933b27e844933060bc"},{"_id":"561771933b27e844933060bd","alpha-2":"AS","name":"American Samoa","countryId":"561771933b27e844933060bd"},{"_id":"561771933b27e844933060be","name":"Andorra","alpha-2":"AD","countryId":"561771933b27e844933060be"},{"_id":"561771933b27e844933060bf","name":"Angola","alpha-2":"AO","countryId":"561771933b27e844933060bf"},{"_id":"561771933b27e844933060c0","alpha-2":"AI","name":"Anguilla","countryId":"561771933b27e844933060c0"},{"_id":"561771933b27e844933060c1","name":"Antarctica","alpha-2":"AQ","countryId":"561771933b27e844933060c1"},{"_id":"561771933b27e844933060c2","name":"Antigua and Barbuda","alpha-2":"AG","countryId":"561771933b27e844933060c2"},{"_id":"561771933b27e844933060c3","name":"Argentina","alpha-2":"AR","countryId":"561771933b27e844933060c3"},{"_id":"561771933b27e844933060c4","alpha-2":"AM","name":"Armenia","countryId":"561771933b27e844933060c4"},{"_id":"561771933b27e844933060c5","name":"Aruba","alpha-2":"AW","countryId":"561771933b27e844933060c5"}];

    var supportedCurrencies = {
        "type" : "CurrencySet",
        "list" : [
            {
                "type":"Currency",
                "code":"GBP",
                "currency":"Pound"
            },{
                "type":"Currency",
                "code":"USD",
                "currency":"US Dollar"
            },{
                "type":"Currency",
                "code":"KR",
                "currency":"Krona"
            },{
                "type":"Currency",
                "code":"INR",
                "currency":"Rupee"
            }
        ]
    };

    var supportedTimeZones = {
        "type" : "TimeZoneSet",
        "count" : 10,
        "totalCount" : 25,
        "list" : [
            {
                "type" : "TimeZone",
                "offset" : "+05:30",
                "description" : "IST"
            },
            {
                "type" : "TimeZone",
                "offset" : "-11:00",
                "description" : "NUT"
            },
            {
                "type" : "TimeZone",
                "offset" : "-08:00",
                "description" : "PST"
            },
            {
                "type" : "TimeZone",
                "offset" : "+00:00",
                "description" : "GMT"
            }
        ]
    };

    beforeEach(module('app'));

    beforeEach(inject(function ($rootScope,$injector, $controller, commonUtilityService, $sce,$q,modalDetails,formMessage) {
        _$controller=$controller;
        _$sce=$sce;
        _$q=$q;
        scope = {};
        _commonUtilityService=commonUtilityService;
        _modalDetails=modalDetails;
        _formMessage = formMessage;
        _rootScope = $rootScope;
        scope.$parent={};
        scope.$parent.enterpriseModalObj={};
        scope.addEnterprise = {}
        scope.addEnterprise.modal ={id: 'addEnterpriseModal'};

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');

    }));

    it('should test addEnterpriseCtrl initialization with modal properties', function () {

        spyOn(_modalDetails, 'attributes');
        spyOn(_modalDetails, 'cancel');
        spyOn(_modalDetails, 'submit');

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        expect(_modalDetails.attributes).toHaveBeenCalledWith({
            id: 'addEnterpriseModal',
            formName: 'addEnterpriseForm',
            cancel: 'Cancel',
            submit: 'Create'
        });

        expect(_modalDetails.cancel).toHaveBeenCalled();
        expect(_modalDetails.submit).toHaveBeenCalled();
    });

    it('should verify the trustAsHtml function', function () {
        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        addEnterpriseCtrl.trustAsHtml();

        var actualMsg = _$sce.getTrustedHtml('ENTERPRISE');
        expect(actualMsg).toBe('ENTERPRISE');

    });


    it('should verify the addEnterpriseCtrl initialization when getSupportedCurrencies & getTimeZones APIs are successful', function () {

        spyOn(_commonUtilityService,"getSupportedCurrencies").and.callFake(function(){
            var expectedResponse = supportedCurrencies;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(_commonUtilityService,"getTimeZones").and.callFake(function(){
            var expectedResponse = supportedTimeZones;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});
        _rootScope.$apply();

        expect(addEnterpriseCtrl.supportedCurrencies).toEqual(supportedCurrencies.list);
        expect(addEnterpriseCtrl.subscriptionTypes).toEqual(['GEM PRO', 'GEM LITE']);
        expect(addEnterpriseCtrl.timeZones).toEqual(supportedTimeZones.list);
    });


    it('should verify the addEnterpriseCtrl initialization when getSupportedCurrencies fails', function () {

        spyOn(_commonUtilityService,"getSupportedCurrencies").and.callFake(function(){
            var errorResponse = {errorStr : 'getSupportedCurrencies failed'};
            var d = _$q.defer();
            d.reject({data:errorResponse});
            return d.promise;
        });

        spyOn(_commonUtilityService,"getTimeZones").and.callFake(function(){
            var expectedResponse = supportedTimeZones;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});
        _rootScope.$apply();

        expect(addEnterpriseCtrl.supportedCurrencies).toBeUndefined();
        expect(addEnterpriseCtrl.subscriptionTypes).toBeUndefined();
        expect(addEnterpriseCtrl.timeZones).toBeUndefined();
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith('getSupportedCurrencies failed');
    });


    it('should verify the addEnterpriseCtrl initialization when getTimeZones fails', function () {

        spyOn(_commonUtilityService,"getSupportedCurrencies").and.callFake(function(){
            var expectedResponse = supportedCurrencies;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(_commonUtilityService,"getTimeZones").and.callFake(function(){
            var errorResponse = {errorStr : 'getTimeZones failed'};
            var d = _$q.defer();
            d.reject(errorResponse);
            return d.promise;
        });

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});
        _rootScope.$apply();

        expect(addEnterpriseCtrl.supportedCurrencies).toBeUndefined();
        expect(addEnterpriseCtrl.subscriptionTypes).toBeUndefined();
        expect(addEnterpriseCtrl.timeZones).toBeUndefined();
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith('getTimeZones failed');
    });

    it('should test updateModal function', function () {

        spyOn(_commonUtilityService,"getSupportedCurrencies").and.callFake(function(){
            var expectedResponse = supportedCurrencies;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(_commonUtilityService,"getTimeZones").and.callFake(function(){
            var expectedResponse = supportedTimeZones;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});
        _rootScope.$apply();

        var modalObj = {
            header: "Add Enterprise",
            submit: "Create",
            cancel: "Cancel"
        };

        spyOn(_modalDetails, 'attributes');

        scope.isFormValid = function(){
            return true;
        };

        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn()
        });

        scope.$parent.enterpriseModalObj.addEnterpriseModal(countries,modalObj,function(){});

        expect(addEnterpriseCtrl.enterprise).toEqual({});
        expect(addEnterpriseCtrl.countries).toEqual(countries);
        expect(addEnterpriseCtrl.errorMessage).toBe('');
        expect(_modalDetails.attributes).toHaveBeenCalledWith(modalObj);

    });

    it('should test updateModal function when getSupportedCurrencies list is not present', function () {

        spyOn(_commonUtilityService,"getSupportedCurrencies").and.callFake(function(){
            var expectedResponse = {list:[]};
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(_commonUtilityService,"getTimeZones").and.callFake(function(){
            var expectedResponse = supportedTimeZones;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});
        _rootScope.$apply();

        var modalObj = {
            header: "Add Enterprise",
            submit: "Create",
            cancel: "Cancel"
        };

        spyOn(_modalDetails, 'attributes');

        scope.isFormValid = function(){
            return true;
        };

        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn()
        });

        scope.$parent.enterpriseModalObj.addEnterpriseModal(countries,modalObj,function(){});

        expect(addEnterpriseCtrl.enterprise).toEqual({});
        expect(addEnterpriseCtrl.countries).toEqual(countries);
        expect(addEnterpriseCtrl.errorMessage).toBe('Unable to process the request. Please reload the page.');
        expect(_modalDetails.attributes).toHaveBeenCalledWith(modalObj);

    });

    it('should test updateModal function when getTimeZones list is not present', function () {

        spyOn(_commonUtilityService,"getSupportedCurrencies").and.callFake(function(){
            var expectedResponse = supportedCurrencies;
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(_commonUtilityService,"getTimeZones").and.callFake(function(){
            var expectedResponse = {list:[]};
            var d = _$q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});
        _rootScope.$apply();

        var modalObj = {
            header: "Add Enterprise",
            submit: "Create",
            cancel: "Cancel"
        };

        spyOn(_modalDetails, 'attributes');

        scope.isFormValid = function(){
            return true;
        };

        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn()
        });

        scope.$parent.enterpriseModalObj.addEnterpriseModal(countries,modalObj,function(){});

        expect(addEnterpriseCtrl.enterprise).toEqual({});
        expect(addEnterpriseCtrl.countries).toEqual(countries);
        expect(addEnterpriseCtrl.errorMessage).toBe('Unable to process the request. Please reload the page.');
        expect(_modalDetails.attributes).toHaveBeenCalledWith(modalObj);

    });

    it('should test reset Modal function', function () {

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});

        spyOn(_formMessage, 'message');
        spyOn(_formMessage, 'scope');

        scope.$parent.enterpriseModalObj.resetEnterpriseModal();
        expect(addEnterpriseCtrl.enterprise).toEqual({});
        expect(_formMessage.scope).toHaveBeenCalledWith( addEnterpriseCtrl);
        expect(_formMessage.message).toHaveBeenCalledWith( false, '');
    });

    it('should test formatTimeZonesDescription', function () {

        addEnterpriseCtrl = _$controller('AddEnterpriseCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService,$rootScope:_rootScope});

        var timeZoneList = angular.copy(supportedTimeZones.list);

        for(var i=0;i<timeZoneList.length;i++){
            delete timeZoneList[i].info;
            expect(timeZoneList[i].info).toBeUndefined();
        }
        addEnterpriseCtrl.formatTimeZonesDescription(timeZoneList);

        for(var j=0;j<timeZoneList.length;j++){
            expect(timeZoneList[j].info).toBe(timeZoneList[j].offset +' ('+ timeZoneList[j].description +')');
        }
    });



});