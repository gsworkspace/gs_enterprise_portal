describe('Authentication service api', function () {

    var _AuthenticationService, $httpBackend,_timeout,_http;
    beforeEach(module('app'));
    beforeEach(inject(function ($injector,$timeout, AuthenticationService,$http) {
        _AuthenticationService = AuthenticationService;
        $httpBackend = $injector.get('$httpBackend');
        _timeout = $timeout;
        _http = $http;

    }));

    it('verify login', function () {

        var getSimsHandler = $httpBackend.expectPOST(/.*?api\/v1\/users\/user\/login.*/g)
            .respond(200, {
                "type": "LoginResponse",
                "token": "16378e50-91a1-11e5-b446-27f5178b71e0",
                "customerId": "562dcda42668cc28027b5c95",
                "enterpriseId": "55efdc6360f02ca22c934e90",
                "expiryDate": "2015-11-24 10:44:35",
                "accountId": "jdande@gigsky.com"
            });
        var emailId = 'jdande@gigsky.com';
        var password = 'Jagadish1234';

        var response = _AuthenticationService.authenticate(emailId, password);

        response.success(function (data) {
            expect(data.type).toEqual("LoginResponse");
        });
        $httpBackend.flush();
        _timeout(function(){
            expect(sessionStorage.getItem("token")).toEqual('16378e50-91a1-11e5-b446-27f5178b71e0');
        });
    });
    it('verify login with out email case', function () {

        var getSimsHandler = $httpBackend.expectPOST(/.*api\/v1\/users\/user\/login.*/g)
            .respond(500, {});
        var emailId = 'jdande@gigsky.com';
        var password = 'Jagadish1234';

        var response = _AuthenticationService.authenticate();

        response.error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });
    it('verify isSessionValid no token', function () {
        _AuthenticationService.clearSession();
        expect(_AuthenticationService.isSessionValid()).toEqual(false);

    });
    it('verify isSessionValid', function () {
        sessionStorage.setItem('token', '12345678910');
        expect(_AuthenticationService.isSessionValid()).toEqual(true);

    });
    it('verify getUserEmailId no emailId', function () {
        _AuthenticationService.clearSession();
        expect(_AuthenticationService.getUserEmailId()).toEqual(null);

    });
    it('verify getUserEmailId', function () {
        sessionStorage.setItem('userEmail', 'jdande@gigsky.com');
        expect(_AuthenticationService.getUserEmailId()).toEqual('jdande@gigsky.com');

    });
    it('verify getUserId no customerId', function () {
        _AuthenticationService.clearSession();
        expect(_AuthenticationService.getUserId()).toEqual(null);

    });
    it('verify getUserId', function () {
        sessionStorage.setItem('userId', '1234');
        expect(_AuthenticationService.getUserId()).toEqual('1234');

    });
    it('verify logout', function () {
        spyOn(_AuthenticationService,'clearSession');
        sessionStorage.setItem('userId', '1234');
        var getSimsHandler = $httpBackend.expectPOST(/.*?api\/v1\/users\/user\/1234\/logout.*/g)
            .respond(200);
        var response = _AuthenticationService.userAccountLogout();

        $httpBackend.flush();
        _timeout(function(){
        expect(_AuthenticationService.clearSession).toHaveBeenCalled();
        });
    });
    it('verify logout failure', function () {
        spyOn(_AuthenticationService,'clearSession');
        sessionStorage.setItem('userId', '1234');
        var getSimsHandler = $httpBackend.expectPOST(/.*?api\/v1\/users\/user\/1234\/logout.*/g)
            .respond(500);
        var response = _AuthenticationService.userAccountLogout();

        $httpBackend.flush();
        _timeout(function(){
            expect(_http.defaults.headers.common.Authorization).toEqual(null);
        });
    });
    it('verify forgotPassword', function () {
        spyOn(_AuthenticationService,'clearSession');
        var getSimsHandler = $httpBackend.expectPOST(/.*?api\/v1\/users\/user\/password.*/g)
            .respond(200);
        var emailId = 'jdande@gigsky.com';
        var response = _AuthenticationService.forgotPassword(emailId);

        $httpBackend.flush();
        _timeout(function(){
            expect(_http.defaults.headers.common.Authorization).toEqual(null);
        });
    });
    it('verify resetPassword', function () {
        spyOn(_AuthenticationService,'clearSession');
        var getSimsHandler = $httpBackend.expectPOST(/.*?api\/v1\/users\/user\/password\/reset.*/g)
            .respond(200);
        var password = 'Jagdish1234';
        var confirmationCode = '1234';
        var response = _AuthenticationService.resetPassword(password,confirmationCode);

        $httpBackend.flush();
    });
    it('verify getUserAccount', function () {
        spyOn(_AuthenticationService,'clearSession');
        var getSimsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/user\/1234.*/g)
            .respond(200);
        var accountId = '1234';
        var response = _AuthenticationService.getUserAccount(accountId);

        $httpBackend.flush();
    });
    it('verify enterpriseLogin', function () {
        spyOn(_AuthenticationService,'clearSession');
        var getSimsHandler = $httpBackend.expectPOST(/.*?api\/v1\/users\/user\/1234\/account\/1234\/login.*/g)
            .respond(200,{"type":"AccountLoginResponse","token":"428ce400-91a1-11e5-b446-27f5178b71e0","expiryDate":"2015-11-24 10:45:49"});
        var getSimsHandler1 = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/1234.*/g)
            .respond(200,{"type":"Account","accountId":1,"accountType":"GIGSKY_ROOT","accountName":"Gigsky","company":{"type":"Company","name":"Gigsky","phone":"1111111111","currency":"INR","timeZone":"+05:30","timeZoneInfo":"IST – India Standard Time","address":{"type":"Address","address1":"Sextor XI, 10th main","address2":"Jeevan bima nagar","state":"Karnataka","city":"Begaluru","zipCode":"560075","country":"IN"}},"alertSetting":{"type":"alertSetting","alertEnabled":false,"alertEmailForAdminEnabled":false,"alertEmailForUserEnabled":false},"accountDescription":"Gigsky Root account"});
        var accountId = '1234';
        var userId = '1234';
        var response = _AuthenticationService.enterpriseLogin(accountId,userId);

        $httpBackend.flush();
        _timeout(function(){
            expect(_http.defaults.headers.common.Authorization).toEqual('428ce400-91a1-11e5-b446-27f5178b71e0');
        });
    });
});